# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError


class Hijos(models.Model):
    _name = 'hijos.hijos'
    _description = 'Objeto Hijos'

    name = fields.Char(string='Nombre', required=True)
    document_number = fields.Char(string='Número de Documento', required=True, unique=True)
    schooling = fields.Selection([('preescolar', 'Preescolar'),
                                  ('primaria', 'Primaria'),
                                  ('bachiller', 'Bachiller'),
                                  ('profesional', 'Profesional'),
                                  ('ninguno', 'Ninguno')], string='Escolaridad')
    birthday = fields.Date(string='Fecha de Nacimiento')
    age = fields.Integer(string='Edad', compute='_compute_age', store=True)
    employee_id = fields.Many2one('hr.employee', string='Empleado')

    _sql_constraints = [
        ('unique_document_number', 'UNIQUE (document_number)', 'El número de documento debe ser único.'),
    ]

    def calculate_age(self, birthday):
        today = fields.Datetime.now()
        age = relativedelta(today, birthday).years
        return age

    @api.depends('birthday')
    def _compute_age(self):
        for children in self:
            if children.birthday:
                children.age = self.calculate_age(children.birthday)


class Employee(models.Model):
    _inherit = 'hr.employee'

    children_ids = fields.One2many('hijos.hijos', 'employee_id', string='Hijos')
    third_partner_id = fields.Many2one('res.partner', string='Tercero')

    def action_open_hijos_form(self):
        return {
            'name': 'Crear Hijo',
            'type': 'ir.actions.act_window',
            'res_model': 'hijos.hijos',
            'view_mode': 'form',
            'view_id': self.env.ref('hijos.view_hijos_form').id,
            'target': 'current',
        }

    @api.onchange('third_partner_id')
    def _onchange_third_partner(self):
        if self.third_partner_id:
            self.name = self.third_partner_id.name
            self.identification_id = self.third_partner_id.vat or ''
            self.mobile_phone = self.third_partner_id.phone or ''
            self.work_email = self.third_partner_id.email or ''

    @api.constrains('third_partner_id')
    def _check_unique_third_partner(self):
        for employee in self:
            if employee.third_partner_id:
                other_employees = self.search(
                    [('id', '!=', employee.id), ('third_partner_id', '=', employee.third_partner_id.id)])
                if other_employees:
                    raise exceptions.ValidationError(
                        'No se puede seleccionar el mismo tercero en dos empleados diferentes.')
